# frozen_string_literal: true

RSpec.describe ApplicationHelper do
  include ApplicationHelper

  describe 'correct title name' do
    context '引数に何も渡さない or nilを渡した場合' do
      it { expect(full_title(' ')).to eq 'BIGBAG store' }
      it { expect(full_title(nil)).to eq 'BIGBAG store' }
      it { expect(full_title).to eq 'BIGBAG store' }
    end

    context '引数を渡す場合' do
      it { expect(full_title('test')).to eq 'test - BIGBAG store' }
    end
  end

  describe 'correct breadcrumbs in the light section' do
    let(:taxonomy) { create :taxonomy }
    let(:product) { create :product, name: 'RUBY ON RAILS MUG' }
    let(:category) do
      create :taxon, taxonomy: taxonomy, name: 'MUGS',
                     parent_id: taxonomy.root.id
    end

    before do
      product.taxons << category
    end

    context '引数にnilを渡した場合' do
      it { expect(taxon_breadcrumbs(nil)).to eq '' }
    end

    context '引数を渡す場合' do
      it { expect(taxon_breadcrumbs(category)).to include 'MUGS' }
      it { expect(taxon_breadcrumbs(product)).to include 'MUGS', 'RUBY ON RAILS MUG' }
    end
  end
end
