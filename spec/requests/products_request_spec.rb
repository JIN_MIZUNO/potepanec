# frozen_string_literal: true

RSpec.describe 'Products', type: :request do
  let(:base_title) { 'BIGBAG store' }
  let(:product) { create :product, taxons: [taxon] }
  let!(:related_products) { create_list(:product, 2, taxons: [taxon]) }
  let(:taxon) { create :taxon, id: 1 }

  describe 'product :show page' do
    before do
      get potepan_product_path product.id
    end

    it 'gets show page' do
      expect(response).to have_http_status :success
    end

    it 'has correct title for show-page' do
      expect(response.body).to match %r{<title>#{product.name} - #{base_title}</title>}i
    end

    describe 'product information' do
      it { expect(response.body).to include product.name }
      it { expect(response.body).to include product.display_price.to_s }
      it { expect(response.body).to include product.description }
    end

    describe 'related_product information' do
      it 'includes related product name' do
        expect(response.body).to include related_products.first.name
        expect(response.body).to include related_products.second.name
      end

      it 'includes related product price' do
        expect(response.body).to include related_products.first.display_price.to_s
        expect(response.body).to include related_products.second.display_price.to_s
      end
    end
  end
end
