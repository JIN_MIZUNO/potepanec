# frozen_string_literal: true

RSpec.describe 'Categories', type: :request do
  let(:base_title) { 'BIGBAG store' }
  let(:taxonomies) { create_list(:taxonomy, 2) }
  let(:product) { create :product, name: 'sample' }
  let(:category) do
    create :taxon, products: [product], taxonomy: taxonomies[0],
                   parent_id: taxonomies[0].root.id
  end

  describe 'categories :show page' do
    before do
      get potepan_category_path category.id
    end

    it 'gets show page' do
      expect(response).to have_http_status :success
    end

    it 'has correct title for show-page' do
      expect(response.body).to match %r{<title>#{category.name} - #{base_title}</title>}i
    end

    it 'includes a sample product' do
      expect(response.body).to include product.name
    end
  end
end
