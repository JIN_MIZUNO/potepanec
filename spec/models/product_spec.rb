# frozen_string_literal: true

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create :taxon, id: 1 }
  let(:another_taxon) { create :taxon, id: 2 }
  let!(:product) { create :product, taxons: [taxon] }
  let!(:related_product) { create_list(:product, 2, taxons: [taxon]) }
  let!(:unrelated_product) { create_list(:product, 3, taxons: [another_taxon]) }

  describe 'related_product' do
    it 'returns all related_product' do
      expect(product.related_products(Constants::RELATED_PRODUCTS_COUNT))
        .to match_array related_product
    end

    context 'when it has many related_products' do
      let!(:many_related_product) { create_list(:product, 10, taxons: [taxon]) }
      it 'returns not more than the Constants of related_product_products' do
        expect(product.related_products(Constants::RELATED_PRODUCTS_COUNT).size)
          .to eq Constants::RELATED_PRODUCTS_COUNT
      end
    end
  end
end
