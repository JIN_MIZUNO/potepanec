# frozen_string_literal: true

RSpec.feature 'Categories', type: :feature do
  given(:taxonomies) { create_list(:taxonomy, 2) }
  given(:category) do
    create :taxon, products: [product], taxonomy: taxonomies[0], name: 'BAGS',
                   parent_id: taxonomies[0].root.id
  end
  given(:another_category) do
    create :taxon, products: [another_product], taxonomy: taxonomies[0], name: 'MUGS',
                   parent_id: taxonomies[0].root.id
  end
  given!(:product) { create :product, name: 'bag', id: 1 }
  given!(:another_product) { create :product, name: 'mug', id: 2 }

  scenario 'click on sample product link' do
    visit potepan_category_path(category.id)
    expect(current_path).to eq potepan_category_path(category.id)
    expect(page).to have_title "#{category.name} - BIGBAG store"
    within '.productsContent' do
      expect(find('#productLink')).to have_link product.name
      expect(find('#productLink')).to have_no_link another_product.name
      click_on product.name
    end
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario 'click on 2nd sample product link' do
    visit potepan_category_path(another_category.id)
    expect(current_path).to eq potepan_category_path(another_category.id)
    expect(page).to have_title "#{another_category.name} - BIGBAG store"
    within '.productsContent' do
      expect(find('#productLink')).to have_link another_product.name
      expect(find('#productLink')).to have_no_link product.name
      click_on another_product.name
    end
    expect(current_path).to eq potepan_product_path(another_product.id)
  end
end
