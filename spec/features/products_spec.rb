# frozen_string_literal: true

RSpec.feature 'Products', type: :feature do
  given(:taxon) { create :taxon, id: 1 }
  given(:another_taxon) { create :taxon, id: 2 }
  given!(:product) { create :product, taxons: [taxon], price: '1' }
  given!(:related_product) { create :product, taxons: [taxon], price: '2' }
  given!(:unrelated_product) { create :product, taxons: [another_taxon], price: '3' }

  scenario 'view product information with related products information' do
    visit potepan_product_path(product.id)
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - BIGBAG store"
    within '.singleProduct' do
      expect(find('.media-body')).to have_content product.name
      expect(find('.media-body')).to have_content product.display_price
      expect(find('.media-body')).to have_content product.description
    end
    within '.productsContent' do
      expect(find('#relatedProducts')).to have_link related_product.name
      expect(find('#relatedProducts')).to have_no_link unrelated_product.name
      expect(find('#relatedProducts')).to have_link related_product.display_price
      expect(find('#relatedProducts')).to have_no_link unrelated_product.display_price
      click_on related_product.name
    end
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
