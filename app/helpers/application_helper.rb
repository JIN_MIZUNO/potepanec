# frozen_string_literal: true

module ApplicationHelper
  BASE_TITLE = 'BIGBAG store'

  def full_title(page_title = '')
    if page_title.blank?
      BASE_TITLE
    else
      "#{page_title} - #{BASE_TITLE}"
    end
  end

  def taxon_breadcrumbs(taxon, separator = '  /  ', breadcrumb_class = 'inline')
    return '' if taxon.nil?

    crumbs = [[t('spree.home'), potepan_url]]

    if taxon.respond_to? :ancestors
      t = taxon.ancestors.first
      crumbs << [t.name, potepan_category_path(t.id)] unless t.nil?
      crumbs << [taxon.name, potepan_category_path(taxon.id)]
    else
      t = taxon.taxons.first
      crumbs << [t.name, potepan_category_path(t.id)]
      crumbs << [taxon.name, potepan_product_path(taxon.id)]
    end

    items = crumbs.collect do |crumb|
      content_tag(:li, itemprop: 'itemListElement', itemscope: '',
                       itemtype: 'https://schema.org/BreadcrumbList') do
        link_to(crumb.last, itemprop: 'item') do
          content_tag(:span, crumb.first, itemprop: 'name')
        end + (crumb == crumbs.last ? '' : separator)
      end
    end

    content_tag(:nav, content_tag(:ul, safe_join(items.map(&:mb_chars)),
                                  class: breadcrumb_class, itemscope: '', itemtype: 'https://schema.org/BreadcrumbList'),
                id: 'breadcrumbs', class: 'sixteen columns')
  end
end
