# frozen_string_literal: true

module ProductsHelper
  def back_to_index(product)
    if product.taxons
      potepan_category_path(product.taxon_ids.first)
    else
      potepan_category_path(1)
    end
  end
end
