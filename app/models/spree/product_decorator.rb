# frozen_string_literal: true

Spree::Product.class_eval do
  def related_products(limit)
    Spree::Product.in_taxons(taxons).includes(master: :default_price).distinct.where.not(id: id).limit(limit)
  end
end
