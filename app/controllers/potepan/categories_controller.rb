# frozen_string_literal: true

class Potepan::CategoriesController < ApplicationController
  def show
    @category = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(:root)
    @products = @category.all_products.includes(master: :default_price)
  end
end
