# frozen_string_literal: true

class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(Constants::RELATED_PRODUCTS_COUNT)
  end
end
